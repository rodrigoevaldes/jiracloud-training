# jiracloud-training

Repositorio con Data Capacitación Jira Cloud


Tema: Jira Cloud Admin
-------

Introducción
------------

* Jira Atlassian.

* Ecosistema Atlassian.

	* Jira.
	* Confluence.
	* Bitbucket.
	* Bamboo.

* Tipos de Depliegue

	*Jira Server.
	*Jira Cloud.

* Sabores de Jira.

	*Jira Core.
	*Jira Software.
	*Jira Service Desk.
	
* Administracion Jira
    * Tareas de Administrador Jira
	* Tipos de Administrador.
		* Site Administrator
		* Administrator
		* Project Administrator.

    * Administración de Usuarios
		* Fuentes de Usuarios.
		    * https://confluence.atlassian.com/cloud/user-provisioning-959305316.html
			* Invitar Usuarios
		* Usuarios Grupos y Roles.
			* Grupos vs Roles
	* Administración de Permisos.
		* Admins
		* Otros.
		
* Proyectos 
	* Que es un proyecto Jira.
	* Creacion de proyectos.
	* Componentes de un proyecto.
		* Issue types
		* Workflows
		* Pantallas.
		* Campos
		* Esquemas de pantalla.
		* Esquema de pantallas de Issue Type.
		* Esquema de Permisos.
		* 
	* Plantillas de Proyectos Default.
	* 
	
* https://confluence.atlassian.com/cloud/get-started-with-atlassian-cloud-products-744721598.html

	* Crear Sitio.
	* Agregar otros productos. Addons.
	* Detalles de Pago.
	* Agregar Site y Products Admins.
